Source: libusb-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 libusb-dev
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/java-team/libusb-java.git
Vcs-Browser: https://salsa.debian.org/java-team/libusb-java
Homepage: http://libusbjava.sf.net

Package: libusb-java-lib
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libusb-java (<= 0.8+ztex20090101-3)
Description: Java wrapper for libusb (native library)
 This Java library wraps the C library libusb (0.1). It is designed to
 support user level applications to access USB devices regardless of
 the operating system.
 .
 This package provides the architecture-dependent component of
 libusb-java.

Package: libusb-java
Architecture: all
Depends: ${misc:Depends}, libusb-java-lib
Description: Java wrapper for libusb
 This Java library wraps the C library libusb (0.1). It is designed to
 support user level applications to access USB devices regardless of
 the operating system.

Package: libusb-java-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: libusb-java
Description: Java wrapper for libusb (documentation)
 This Java library wraps the C library libusb (0.1). It is designed to
 support user level applications to access USB devices regardless of
 the operating system.
 .
 The libusb source code provides its own documentation. It
 is transformed to HTML by the javadoc tool.
